use crate::ast::{Expr, Stmt};
use crate::callable::Clock;
use crate::environment::Environment;
use crate::object::Object;
use crate::token::Token;
use crate::token::TokenType;
use crate::Lox;


use std::error;
use std::fmt;


pub struct Interpreter {
    environment: Environment,
    globals: Environment,
}

impl Interpreter {
    pub fn new() -> Interpreter {
        let mut globals = Environment::new();
        globals.define(&"clock".to_string(), Object::Callable(Box::new(Clock {})));

        Interpreter {
            environment: globals.clone(),
            globals: globals.clone(),
        }
    }

    pub fn interpret(&mut self, statements: Vec<Box<Stmt>>) {
        for statement in statements {
            self.interpret_statement(statement).unwrap();
        }
    }

    fn interpret_statement(
        &mut self,
        statement: Box<Stmt>,
    ) -> Result<Option<Object>, RuntimeError> {
        match *statement {
            Stmt::Expr(expr) => {
                let expression = self.evaluate(expr);
                match expression {
                    Ok(l) => {
                        // match l.clone() {
                        //     Literal::Bool(b) => {
                        //         println!("{}", b);
                        //     }
                        //     Literal::Number(n) => {
                        //         println!("{}", n);
                        //     }
                        //     Literal::String(s) => {
                        //         println!("\"{}\"", s);
                        //     }
                        //     Literal::Nil => {
                        //         println!("nil");
                        //     }
                        // };
                        // None
                        Ok(Some(l))
                    }
                    Err(e) => {
                        Lox::runtime_error(e);
                        Ok(None)
                    }
                }
            }
            Stmt::While(condition, body) => {
                while Interpreter::is_truthy(self.evaluate(condition.clone())?) {
                    self.interpret_statement(body.to_owned())?;
                }
                Ok(None)
            }

            Stmt::If(condition, then_branch, else_branch) => {
                if Interpreter::is_truthy(self.evaluate(condition).unwrap()) {
                    self.interpret_statement(then_branch)
                } else if else_branch.is_some() {
                    self.interpret_statement(else_branch.unwrap())
                } else {
                    Ok(None)
                }
            }
            Stmt::Block(s) => {
                self.environment = Environment::from(self.environment.clone());
                for statement in s.iter() {
                    self.interpret_statement(Box::new(statement.clone()))
                        .unwrap();
                }

                if let Some(enclosing) = self.environment.enclosing.clone() {
                    self.environment = enclosing.clone().take();
                }
                Ok(None)
            }
            Stmt::Print(expr) => {
                let value = self.evaluate(expr);
                match value {
                    Ok(l) => {
                        match l {
                            Object::Bool(b) => {
                                println!("{}", b);
                            }
                            Object::Number(n) => {
                                println!("{}", n);
                            }
                            Object::String(s) => {
                                println!("\"{}\"", s);
                            }
                            Object::Nil => {
                                println!("nil");
                            }
                            Object::Callable(f) => {
                               println!("{:?}", f.call().unwrap())
                            }
                        };
                        Ok(None)
                    }
                    Err(e) => {
                        Lox::runtime_error(e);
                        Ok(None)
                    }
                }
            }
            Stmt::Var(name, initializer) => {
                let mut value: Object = Object::Nil;
                match *initializer {
                    Some(e) => {
                        let f = self.evaluate(Box::new(e));
                        match f {
                            Ok(e) => value = e,
                            Err(e) => Lox::runtime_error(e),
                        }
                    }
                    None => {}
                }
                self.environment.define(&name.lexeme, value);
                Ok(None)
            }
        }
    }

    fn evaluate(&mut self, expr: Box<Expr>) -> Result<Object, RuntimeError> {
        match *expr {
            Expr::Object(literal) => self.evaluate_literal(literal.to_owned()),
            Expr::Logical(lhs, op, rhs) => {
                let left = self.evaluate(lhs)?;
                if op.token_type == TokenType::Or {
                    if Interpreter::is_truthy(left.clone()) {
                        return Ok(left);
                    }
                } else {
                    if !Interpreter::is_truthy(left.clone()) {
                        return Ok(left);
                    }
                }
                self.evaluate(rhs)
            }
            Expr::Unary(op, e) => self.evaluate_unary(op.to_owned(), e),
            Expr::Binary(lhs, op, rhs) => self.evaluate_binary(lhs, op.to_owned(), rhs),
            Expr::Grouping(e) => self.evaluate(e),
            Expr::Variable(e) => self.environment.get(e),
            Expr::Assignment(t, e) => {
                let value = self.evaluate(e)?;
                self.environment.assign(t, value.clone()).unwrap();
                Ok(value)
            }
            Expr::Call(c, t, a) => {
                let callee  = self.evaluate(c)?;

                let mut args: Vec<Expr> = Vec::new();
                for arg in a {
                    args.push(arg);
                }

                match callee {
                    Object::Callable(func) => {
                        if args.len() != func.arity() {
                            Err(RuntimeError::new(t, format!("Expected {}, arguements but got {}", args.len(), func.arity())))
                        } else {
                            Ok(func.call().unwrap())
                        }
                    }
                    _ => Err(RuntimeError::new(t, "Can only call functions and classes.".to_string()))
                }

            }
        }
    }

    fn evaluate_literal(&mut self, expr: Object) -> Result<Object, RuntimeError> {
        Ok(expr)
    }

    fn evaluate_unary(&mut self, op: Token, expr: Box<Expr>) -> Result<Object, RuntimeError> {
        let right = self.evaluate(expr)?;

        match op.token_type {
            TokenType::Bang => Ok(Object::Bool(Interpreter::is_truthy(right))),
            TokenType::Minus => match right {
                Object::Number(f) => Ok(Object::Number(f * -1 as f64)),
                _ => Err(RuntimeError::new(
                    op,
                    "Invalid negation operand.".to_owned(),
                )),
            },
            _ => Err(RuntimeError::new(op, "Invalid unary operand.".to_owned())),
        }
    }

    fn evaluate_binary(
        &mut self,
        left: Box<Expr>,
        op: Token,
        right: Box<Expr>,
    ) -> Result<Object, RuntimeError> {
        let lhs: Object = self.evaluate(left)?;
        let rhs: Object = self.evaluate(right)?;

        match op.token_type {
            TokenType::Greater => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Bool(lhs > rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::GreaterEqual => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Bool(lhs >= rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::LessEqual => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Bool(lhs <= rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::EqualEqual => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Bool(lhs == rhs)),
                (Object::String(lhs), Object::String(rhs)) => Ok(Object::Bool(lhs == rhs)),
                (Object::String(_), Object::Number(_)) => Ok(Object::Bool(false)),
                (Object::Number(_), Object::String(_)) => Ok(Object::Bool(false)),
                (Object::Bool(lhs), Object::Bool(rhs)) => Ok(Object::Bool(lhs == rhs)),
                (Object::Nil, Object::Nil) => Ok(Object::Bool(true)),
                _ => Ok(Object::Bool(false)),
            },
            TokenType::Less => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Bool(lhs < rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::Minus => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Number(lhs - rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::Plus => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => {
                    Ok(Object::Number(lhs.clone() + rhs.clone()))
                }
                (Object::String(lhs), Object::String(rhs)) => Ok(Object::String(lhs + &rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be either two numbers or two strings.".to_owned(),
                )),
            },
            TokenType::Slash => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Number(lhs / rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            TokenType::Star => match (lhs, rhs) {
                (Object::Number(lhs), Object::Number(rhs)) => Ok(Object::Number(lhs * rhs)),
                _ => Err(RuntimeError::new(
                    op,
                    "Operands must be numbers.".to_owned(),
                )),
            },
            _ => Err(RuntimeError::new(
                op,
                "Operands must be either numbers or strings.".to_owned(),
            )),
        }
    }

    fn is_truthy(value: Object) -> bool {
        match value {
            Object::Nil => false,
            Object::Bool(b) => b,
            _ => true,
        }
    }
}

#[derive(Debug, Clone)]
pub struct RuntimeError {
    pub token: Token,
    pub message: String,
}

impl RuntimeError {
    pub fn new(token: Token, message: String) -> Self {
        RuntimeError {
            token,
            message: message.to_owned(),
        }
    }
}

impl error::Error for RuntimeError {}

impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}", self.token.to_string(), self.message)
    }
}
